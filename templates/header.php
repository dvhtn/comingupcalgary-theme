<header class="banner">
  <div class="container">
    <nav class="banner__nav row">
      <div class="nav-primary col-xs-6">
        <?php
        if ( has_nav_menu( 'primary_navigation' ) ) {
          wp_nav_menu( [ 'theme_location' => 'primary_navigation', 'menu_class' => 'nav' ] );
        }
        ?>
      </div>
      <div class="nav-secondary col-xs-6">
        <?php
        if ( has_nav_menu( 'primary_navigation' ) ) {
          wp_nav_menu( [ 'theme_location' => 'secondary_navigation', 'menu_class' => 'nav' ] );
        }
        ?>
      </div>
    </nav>

    <div class="banner__content">
      <h1><a href="<?php echo home_url(); ?>/"><?php bloginfo( 'name' ); ?></a></h1>
      <hr>
      <div class="description"><?php bloginfo( 'description' ); ?></div>
    </div>
  </div>
</header>
