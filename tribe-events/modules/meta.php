<?php
/**
 * Single Event Meta Template
 *
 * Here, we've gutted the meta so it:
 *
 *  - assumes we're in skeleton mode (no additional div wrappers)
 *  - does not care about keeping the venue apart
 *  - doesn't include the map (we add it under the content)
 *
 * @package TribeEventsCalendar
 */

do_action( 'tribe_events_single_meta_before' );

?>

<?php
do_action( 'tribe_events_single_event_meta_primary_section_start' );

// Always include the main event details in this first section
tribe_get_template_part( 'modules/meta/details' );

// Always print venue
tribe_get_template_part( 'modules/meta/venue' );

// Include organizer meta if appropriate
if ( tribe_has_organizer() ) {
	tribe_get_template_part( 'modules/meta/organizer' );
}

do_action( 'tribe_events_single_event_meta_primary_section_end' );
?>

<?php
do_action( 'tribe_events_single_event_meta_secondary_section_start' );
do_action( 'tribe_events_single_event_meta_secondary_section_end' );
?>
<?php do_action( 'tribe_events_single_meta_after' ); ?>
