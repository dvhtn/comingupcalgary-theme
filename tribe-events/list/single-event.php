<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_meta( 'tribe_event_venue_name' );

// Venue
$has_venue_address = ( ! empty( $venue_details[ 'address' ] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

// Genre
$genre_details = tribe_get_event_categories( );

?>

<!-- Date -->
<span class="cuc-event__date">
  <span class="cuc-event__date__short">
    <?php printf('%s', tribe_get_start_date(null, false, 'D M d')) ?>
  </span>
  <span class="cuc-event__date__full">
    <?php echo tribe_events_event_schedule_details() ?>
  </span>
</span>

<!-- Title -->
<span class="cuc-event__title">
  <?php do_action( 'tribe_events_before_the_event_title' ) ?>
  <span class="tribe-events-list-event-title">
    <a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
      <?php the_title() ?>
    </a>
  </span>
  <?php do_action( 'tribe_events_after_the_event_title' ) ?>
</span>

<span class="cuc-event__genre">
  <?php echo $genre_details; ?>
</span>

<span class="cuc-event__venue">
  <?php echo $venue_details; ?>
</span>
