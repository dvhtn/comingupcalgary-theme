<?php
/**
 * List View Loop
 * This file sets up the structure for the list loop
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/loop.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<?php
global $post;
global $more;
$more = false;
?>

<ul class="cuc-eventlist">
  <li class="cuc-event cuc-event_header">
    <div class="cuc-event__date">Date</div>
    <div class="cuc-event__title">Event</div>
    <div class="cuc-event__genre">Genre</div>
    <div class="cuc-event__venue">Venue</div>
  </li>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php do_action( 'tribe_events_inside_before_loop' ); ?>

    <!-- Event  -->
		<?php
		$post_parent = '';
		if ( $post->post_parent ) {
			$post_parent = ' data-parent-post-id="' . absint( $post->post_parent ) . '"';
		}
		?>
		<li id="post-<?php the_ID() ?>" class="cuc-event <?php tribe_events_event_classes() ?>" <?php echo $post_parent; ?> data-href="<?php echo esc_url( tribe_get_event_link() ); ?>">
			<?php tribe_get_template_part( 'list/single', 'event' ) ?>
		</li>

		<?php do_action( 'tribe_events_inside_after_loop' ); ?>
	<?php endwhile; ?>

</ul><!-- .tribe-events-loop -->
