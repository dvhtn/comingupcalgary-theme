<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<article id="tribe-events-content" class="tribe-events-single cuc-event container">
  <p class="tribe-events-back">
    <a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html__( 'All %s', 'the-events-calendar' ), $events_label_plural ); ?></a>
  </p>

  <div class="row">
    <div class="col-xs-12">
      <header class="cuc-event__header">
        <?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>

        <div class="tribe-events-schedule tribe-clearfix cuc-event__schedule">
          <?php echo tribe_events_event_schedule_details( $event_id, '<span class="tribe-events-schedule-details">', '</span>' ); ?>

          <?php if ( tribe_get_cost() ) : ?>
            <span class="tribe-events-cost cuc-event__cost"><?php echo tribe_get_cost( null, true ) ?></span>
          <?php endif; ?>
        </div>
      </header>
    </div>
  </div>

  <?php while ( have_posts() ) :  the_post(); ?>
    <div class="row">
      <div class="col-md-8 col-md-push-4">
        <section id="post-<?php the_ID(); ?>" <?php post_class(['cuc-event__content']); ?>>
          <?php tribe_the_notices() ?>

          <!-- Event content -->
          <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
          <div class="tribe-events-single-event-description tribe-events-content">
            <?php the_content(); ?>
          </div>
          <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
        </section> <!-- #post-x -->
      </div>

      <div class="col-md-4 col-md-pull-8">
        <aside class="cuc-event__meta">
          <!-- Event featured image, but exclude link -->
          <?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

          <!-- Event meta -->
          <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
          <?php tribe_get_template_part( 'modules/meta' ); ?>
          <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
        </aside>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="cuc-event__map">
          <!-- Map -->
          <?php tribe_get_template_part( 'modules/meta/map' ); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="cuc-event__comments">
          <!-- Comments -->
          <?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
        </div>
      </div>
    </div>

  <?php endwhile; ?>

</article><!-- #tribe-events-content -->
