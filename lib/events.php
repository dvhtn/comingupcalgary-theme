<?php

if ( ! class_exists( 'Tribe__Events__Main' ) ) {
  return;
}

if ( ! function_exists( 'cuc_after_setup_theme' ) ) {
  function cuc_after_setup_theme() {
    $tem = Tribe__Events__Main::instance();

    // remove then re-add the tribe-events filters with the priorities we want
    remove_all_filters( 'tribe-events-bar-filters', 1 );
    add_filter( 'tribe-events-bar-filters', [ $tem, 'setup_date_search_in_bar' ], 20, 1 );
    add_filter( 'tribe-events-bar-filters', [ $tem, 'setup_keyword_search_in_bar' ], 10, 1 );

    // disable the ical link on the events list
    add_filter( 'tribe_events_list_show_ical_link', '__return_false' );
  }

  add_action( 'after_setup_theme', 'cuc_after_setup_theme', 50 );
}

