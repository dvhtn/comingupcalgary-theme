(function($) {
  var ComingUpCalgaryUtils = {
    styleMaps: function() {
      // give maps a gray-scale theme
      // from https://snazzymaps.com/style/151/ultra-light-with-labels
      var mapStyle = [
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e9e9e9"
            },
            {
              "lightness": 17
            }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            },
            {
              "lightness": 20
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "lightness": 17
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "lightness": 29
            },
            {
              "weight": 0.2
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "lightness": 18
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "lightness": 16
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            },
            {
              "lightness": 21
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#dedede"
            },
            {
              "lightness": 21
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "visibility": "on"
            },
            {
              "color": "#ffffff"
            },
            {
              "lightness": 16
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "saturation": 36
            },
            {
              "color": "#333333"
            },
            {
              "lightness": 40
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f2f2f2"
            },
            {
              "lightness": 19
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#fefefe"
            },
            {
              "lightness": 20
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#fefefe"
            },
            {
              "lightness": 17
            },
            {
              "weight": 1.2
            }
          ]
        }
      ];

      // append a new document ready handler so we can set the style right after tribe events initializes the map
      if (window.google) {
        $(window).ready(function () {
          var mapType = new window.google.maps.StyledMapType(mapStyle, {name: "Grayscale"});

          if (tribeEventsSingleMap && tribeEventsSingleMap.addresses) {
            $.each(tribeEventsSingleMap.addresses, function (index, venue) {
              console.log(venue.map);
              if (venue && venue.map) {
                venue.map.mapTypes.set('grayscale', mapType);
                venue.map.setMapTypeId('grayscale');
              }
            });
          }
        });
      }
    }
  };

  var ComingUpCalgary = {
    tribe_events_filter_view: {
      init: function() {
        // add custom text to filters
        $('select[name=tribe_eventcategory] option:first-child').text('ANY GENRE');
        $('select[name=tribe_venues] option:first-child').text('ANY VENUE');

        // navigate to event when clicking on events in the list
        $(document).on('click', '.cuc-event', function(e) {
          var href = $(this).data('href');
          if (href) {
            window.location.href = href;
            return false;
          }
        });
      }
    },

    tribe_events_venue: {
      init: function() {
        ComingUpCalgaryUtils.styleMaps();
      }
    },

    events_single: {
      init: function() {
        ComingUpCalgaryUtils.styleMaps();
      }
    }
  };

  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = ComingUpCalgary;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      UTIL.fire('common');
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });
      UTIL.fire('common', 'finalize');
    }
  };

  $(document).ready(UTIL.loadEvents);
})(jQuery);
